/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.content.Context;

public class AttributeLookup {
    private Map<UUID, String> mUuidMap;
    private Context mContext = null;

    public AttributeLookup(Context context) {
        mContext = context;
        mUuidMap = new HashMap<UUID, String>();

        String[] uuids = context.getResources().getStringArray(R.array.uuids);
        for (String entry : uuids) {
            String[] parts = entry.split("\\|");
            if (parts.length == 2) mUuidMap.put(UUID.fromString(parts[0]), parts[1]);
        }
    }

    public String getService(UUID uuid) {
        if (mUuidMap.containsKey(uuid)) return mUuidMap.get(uuid);
        return mContext.getString(R.string.unknown_service);
    }

    public String getCharacteristic(UUID uuid) {
        if (mUuidMap.containsKey(uuid)) return mUuidMap.get(uuid);
        return mContext.getString(R.string.unknown_char);
    }
}
