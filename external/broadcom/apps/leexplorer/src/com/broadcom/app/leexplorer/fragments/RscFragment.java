/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;

public class RscFragment extends ValueFragment {
    private static final int FLAG_HAS_STRIDE = 0x01;
    private static final int FLAG_HAS_TOTAL = 0x02;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rsc, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        if (value.length < 4) return;

        int offset = 0;
        int flags = DataUtils.unsignedByteToInt(value[offset++]);

        // Parse data

        Float speed = (float)DataUtils.unsignedBytesToInt(value[offset++], value[offset++]) / 256;
        Integer rpm = DataUtils.unsignedByteToInt(value[offset++]);

        // Enable measurement views

        setVisible(R.id.speedMeasurement);
        setVisible(R.id.cadenceMeasurement);

        // Assign values

        NumberFormat df = new DecimalFormat("0.###");
        setText(R.id.textSpeed, df.format(speed));
        setText(R.id.textCadence, rpm.toString());

        if (isSet(flags, FLAG_HAS_STRIDE)) {
            Integer stride = DataUtils.unsignedBytesToInt(value[offset++], value[offset++]);
            setVisible(R.id.strideMeasurement);
            setText(R.id.textStride, stride.toString());
        }

        if (isSet(flags, FLAG_HAS_TOTAL)) {
            Float distance = (float)DataUtils.unsignedBytesToInt(value[offset++], value[offset++], value[offset++], value[offset++]) / 10000;
            setVisible(R.id.distanceMeasurement);
            setText(R.id.textDistance, df.format(distance));
        }
    }

    private boolean isSet(int flags, int flag) {
        return ((flags & flag) != 0);
    }
}
