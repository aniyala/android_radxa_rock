/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;
import com.broadcom.bt.gatt.BluetoothGattCharacteristic;

public class AlertLevelFragment extends ValueFragment implements OnItemSelectedListener {
    private int mAlertLevel = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_alertlevel, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Spinner alertType = (Spinner)findViewById(R.id.spinTyoe);
        alertType.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        mAlertLevel = pos;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    @Override
    public void setReadonly(boolean readonly) {
        Spinner alertType = (Spinner)findViewById(R.id.spinTyoe);
        if (alertType != null) alertType.setEnabled(!readonly);
    }

    @Override
    public void setValue(byte[] value) {
        mAlertLevel = DataUtils.unsignedByteToInt(value[0]);
        Spinner alertType = (Spinner)findViewById(R.id.spinTyoe);
        if (alertType != null) alertType.setSelection(mAlertLevel);
    }

    @Override
    public boolean assignValue(BluetoothGattCharacteristic characteristic) {
        characteristic.setValue(mAlertLevel, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        return true;
    }
}
